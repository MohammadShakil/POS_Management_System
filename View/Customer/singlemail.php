<?php
/**
 * This example shows settings to use when sending via Google's Gmail servers.
 */
//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
date_default_timezone_set('Etc/UTC');
include_once ('../../vendor/phpmailer/phpmailer/class.phpmailer.php');


include_once("../../vendor/autoload.php");
use App\CustomerCreate\CustomerCreate;
use App\Message\Message;
use App\Utility\Utility;
session_start();

$book= new CustomerCreate();
$singleBook=$book->prepare($_GET)->view();



$html=<<<Mamun
/*<ul class="list-group">
        <li class="list-group-item">Name: $singleBook[supplier_name] </li>
        <li class="list-group-item">Address: $singleBook[supplier_address] </li>
        <li class="list-group-item">Phone: $singleBook[supplier_phone]</li>
        <li class="list-group-item">email: $singleBook[supplier_email]</li>
        <li class="list-group-item">Bank Account No: $singleBook[supplier_bank_account]</li>
        
        

    </ul>*/
    
    
    <html>
    <body>
    Hello Mr.$singleBook[customer_name]
    <table width="115" align="left" border="0" cellpadding="0" cellspacing="0">  
      <tr>
        <td height="115" style="padding: 0 20px 20px 0;">
          <img src="images/article1.png" width="115" height="115" border="0" alt="" />
        </td>
      </tr>
    </table>
    <table class="col380" align="left" border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 380px;">  
      <tr>
        <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="bodycopy">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus adipiscing felis, sit amet blandit ipsum volutpat sed. Morbi porttitor, eget accumsan dictum, nisi libero ultricies ipsum, in posuere mauris neque at erat.
              </td>
            </tr>
            <tr>
              <td style="padding: 20px 0 0 0;">
                <table class="buttonwrapper" bgcolor="#e05443" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="button" height="45">
                      <a href="#">Claim yours!</a>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
</body>
</html>



Mamun;



//Create a new PHPMailer instance
$mail = new PHPMailer;
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 0;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';
// use
// $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;
//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "";
//Password to use for SMTP authentication
$mail->Password = "";
//Set who the message is to be sent from
$mail->setFrom('from@example.com', 'First Last');
//Set an alternative reply-to address
$mail->addReplyTo('replyto@example.com', 'First Last');
//Set who the message is to be sent to
$mail->addAddress($singleBook[supplier_email], 'pos Team');
//Set the subject line
$mail->Subject = 'Boot data test';
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
//Replace the plain text body with one created manually
$mail->AltBody = 'This is a plain-text message body';
//Attach an image file
///$mail->addAttachment('images/phpmailer_mini.png');
//send the message, check for errors
echo $html;
$mail->Body=$html;
if (!$mail->send()) {
    Message::message("<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Mail Send Failed.
</div>");
    Utility::redirect('supplier-list.php');
} else {
    Message::message("<div class=\"alert alert-info\">
  <strong>successfully!</strong> Mail Send successfully.
</div>");
    Utility::redirect('supplier-list.php');

}