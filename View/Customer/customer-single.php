<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="assets/images/favicon_1.ico">

        <title>Ubold - Responsive Admin Dashboard Template</title>
        
        <link href="assets/plugins/c3/c3.min.css" rel="stylesheet" type="text/css"  />
        
        <link href="assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet">

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>
        
    </head>


    <body class="fixed-left">
        
        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <?php include_once('header.php'); ?>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->

            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <?php include_once('menu.php'); ?>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- Left Sidebar End --> 



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->                      
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title">Opportunities</h4>
                                
                            </div>
                        </div>
                        
                        <div class="row">
                        	<div class="col-md-6">
                        		<form role="form">
                                    <div class="form-group contact-search m-b-30">
                                    	<input type="text" id="search" class="form-control" placeholder="Search...">
                                        <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
                                    </div> <!-- form-group -->
                                </form>
                        	</div>
                        	<div class="col-md-6">
                        		<a href="#custom-modal" class="btn btn-default btn-md waves-effect waves-light m-b-30 pull-right" data-animation="fadein" data-plugin="custommodal" 
                                                    	data-overlaySpeed="200" data-overlayColor="#36404a"><i class="md md-add"></i> Add New</a>
                        		
                        		 
                        	</div>
                        </div>
                        
                        <div class="row">
                        	<div class="col-lg-12">
                        		 
                        		<div class="card-box m-b-10">
                        			<div class="table-box opport-box">
                        				
                        				
                        				<div class="table-detail">
                        					<div class="member-info">
	                                            <h4 class="m-t-0"><b>Enveto Market Pty Ltd. </b></h4>
	                                            <p class="text-dark m-b-5"><b>Category: </b> <span class="text-muted">Branch manager</span></p>
	                                            <p class="text-dark m-b-0"><b>Active: </b> <span class="text-muted">2 hours ago</span></p>
	                                        </div>
                        				</div>
                        				
                        				<div class="table-detail">
                        					<p class="text-dark m-b-5"><b>Email:</b> <span class="text-muted">coderthemes@gmail.com</span></p>
                        					<p class="text-dark m-b-0"><b>Contact:</b> <span class="text-muted">+12 34567890</span></p>
                        				</div>
                        				
                        				<div class="table-detail lable-detail">
                        					<span class="label label-info">Hot</span>
                        				</div>
                        				
                        				<div class="table-detail table-actions-bar">
                        					<a href="#" class="table-action-btn"><i class="md md-edit"></i></a>
                                        	<a href="#" class="table-action-btn"><i class="md md-close"></i></a>
                        				</div>
                        			</div>
                        		</div>
                        		
                        		
                        		
                        		
                        		
                        		
                        		
                        		
                        		
                        		
                        		
                        		
                        		
                        		
                                
                            </div> <!-- end col -->
                         
                            
                        </div>

                        
                        
                        

                    </div> <!-- container -->
                               
                </div> <!-- content -->

                <?php include_once('footer.php'); ?>

            </div>
            
            
            <!-- Modal -->
			<div id="custom-modal" class="modal-demo">
			    <button type="button" class="close" onclick="Custombox.close();">
			        <span>&times;</span><span class="sr-only">Close</span>
			    </button>
			    <h4 class="custom-modal-title">Add New</h4>
			    <div class="custom-modal-text text-left">
			        <form role="form">
			        	<div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Enter name">
                        </div>
                        
                        <div class="form-group">
                            <label for="position">Category</label>
                            <input type="text" class="form-control" id="position" placeholder="Enter category">
                        </div>
                        
                        <div class="form-group">
                            <label for="position1">Contact number</label>
                            <input type="text" class="form-control" id="position1" placeholder="Enter number">
                        </div>
                        
                        
                        <button type="submit" class="btn btn-default waves-effect waves-light">Save</button>
                        <button type="button" class="btn btn-danger waves-effect waves-light m-l-10">Cancel</button>
                    </form>
			    </div>
			</div>
            
            
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            
            <!-- /Right-bar -->

        </div>
        <!-- END wrapper -->


    
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>


        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
        
        <!-- Modal-Effect -->
        <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
        <script src="assets/plugins/custombox/dist/legacy.min.js"></script>
        
        <!--C3 Chart-->
        <script type="text/javascript" src="assets/plugins/d3/d3.min.js"></script>
        <script type="text/javascript" src="assets/plugins/c3/c3.min.js"></script>
        
        <script type="text/javascript" src="assets/pages/jquery.opportunities.init.js"></script>
        

       
    
    </body>
</html>