<?php
session_start();
include_once("../../vendor/autoload.php");
use App\Supplier\Supplier;
use App\Message\Message;
use App\Utility\Utility;

$supplier= new Supplier();
var_dump($_GET);

$getAllSupplier =$supplier->index();
/*var_dump($getAllSupplier);
die();*/

$allName=$supplier->getAllName();
$comma_separated_name= '"'.implode('","',$allName).'"';

if(strtoupper($_SERVER['REQUEST_METHOD']=="GET")){
    $getAllSupplier =$supplier->index();
}
/*if(strtoupper($_SERVER['REQUEST_METHOD']=="POST")){
    $getAllSupplier =$supplier->prepare();
}*/
if((strtoupper($_SERVER['REQUEST_METHOD']=="GET"))&& isset($_GET['search'])){
    $getAllSupplier =$supplier->prepare($_GET)->index();
}

?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <link rel="shortcut icon" href="../assets/images/favicon_1.ico">

    <title>Ubold - Responsive Admin Dashboard Template</title>

    <link href="../assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet">

    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="../assets/js/modernizr.min.js"></script>
    <!----CDN FOR SEARCH--->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

</head>


<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <?php include_once('../header.php'); ?>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->

    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
            <!--- Divider -->
            <?php include_once('../menu.php'); ?>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- Left Sidebar End -->



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Suppliers</h4>
                        <ol class="breadcrumb">
                            <li><a href="#">POS</a></li>
                            <li><a href="#">Apps</a></li>
                            <li class="active">Supplier</li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-sm-8">
                                    <form role="form" action="supplier-list.php" method="get">
                                        <div class="form-group contact-search m-b-30">
                                            <input type="text" name="" id="search" class="form-control" placeholder="Search...">
                                            <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
                                        </div> <!-- form-group -->
                                    </form>
                                </div>
                                <div class="col-sm-4">
                                    <a href="#custom-modal" class="btn btn-default btn-md waves-effect waves-light m-b-50" data-animation="fadein" data-plugin="custommodal"
                                       data-overlaySpeed="200" data-overlayColor="#36404a"><i class="md md-add"></i> Add Supplier</a>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-hover mails m-0 table table-actions-bar">
                                    <thead>
                                    <div id="message">
                                        <?php
                                        if((array_key_exists("message",$_SESSION))&&(!empty($_SESSION["message"]))) {
                                            echo Message::message();
                                        }
                                        ?>
                                    </div>


                                    <tr>
                                        <th>SL#</th>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Bank Info</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $sl=0;
                                    foreach($getAllSupplier as $supplier){
                                        $sl++;
                                        ?>
                                        <tr>
                                            <td><?php echo $sl ?></td>
                                            <td><?php echo $supplier['supplier_id'] // for object: $book->id ; ?></td>
                                            <td><?php echo $supplier['supplier_name'] // for object: $book->title; ?></td>
                                            <td><?php echo $supplier['supplier_address'] // for object: $book->title; ?></td>
                                            <td><?php echo $supplier['supplier_email'] // for object: $book->title; ?></td>
                                            <td><?php echo $supplier['supplier_phone'] // for object: $book->title; ?></td>
                                            <td><?php echo $supplier['supplier_bank_account'] // for object: $book->title; ?></td>
                                            <td>
                                                <a href="edit.php?supplier_id=<?php echo $supplier['supplier_id']?> " class="table-action-btn"><i class="md md-edit"></i></a>
                                                <a href="delete.php?supplier_id=<?php echo $supplier['supplier_id']?>  " class="table-action-btn"><i class="md md-close"></i></a>
                                                <a href="singlemail.php?supplier_id=<?php echo $supplier['supplier_id']?>  " class="table-action-btn"><i class="md-email"></i></a>
                                            </td>


                                        </tr>
                                    <?php } ?>


                                    </tbody>
                                </table>

                            </div>
                        </div>


                        </table>
                        </table>
                    </div>
                </div>
            </div> <!-- end col -->
        </div>

    </div> <!-- container -->

</div> <!-- content -->

<?php include_once('../footer.php'); ?>

</div>


<!-- Modal -->
<div id="custom-modal" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Add Supplier</h4>
    <div class="custom-modal-text text-left">
        <form class="form-horizontal" method="post" role="form" action="store.php">
            <div class="form-group">
                <label class="col-md-2 control-label">Name</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="supplier_name" value="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Address</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="supplier_address"value="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Phone</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="supplier_phone" value="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label" for="example-email">Email</label>
                <div class="col-md-10">
                    <input type="email" id="example-email" name="supplier_email" class="form-control" placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Bank Info</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="supplier_bank_account" value="">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Stauts</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="supplier_status" value="">
                </div>
            </div>

            <div class="form-group ">
                <div class="col-sm-offset-2 col-sm-9">
                    <button type="submit" class="btn btn-info waves-effect waves-light">Submit</button>
                </div>
            </div>

        </form>
    </div>
</div>


<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->


<!-- Right Sidebar -->

<!-- /Right-bar -->

</div>
<!-- END wrapper -->



<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/detect.js"></script>
<script src="../assets/js/fastclick.js"></script>
<script src="../assets/js/jquery.slimscroll.js"></script>
<script src="../assets/js/jquery.blockUI.js"></script>
<script src="../assets/js/waves.js"></script>
<script src="../assets/js/wow.min.js"></script>
<script src="../assets/js/jquery.nicescroll.js"></script>
<script src="../assets/js/jquery.scrollTo.min.js"></script>


<script src="../assets/js/jquery.core.js"></script>
<script src="../assets/js/jquery.app.js"></script>

<!-- Modal-Effect -->
<script src="../assets/plugins/custombox/dist/custombox.min.js"></script>
<script src="../assets/plugins/custombox/dist/legacy.min.js"></script>

<script>
    $( function() {
        var Name = [
            <?php echo $comma_separated_name ?>
        ];

        $( "#search" ).autocomplete({
            source: Name
        });
    } );

    <!--Script Below For Message-->
    $('#message').show().delay(3000).fadeOut();
</script>

</body>
</html>