<?php
include_once('../../vendor/autoload.php');
use App\ProductSize\ProductSize;

$product_size = new ProductSize();

$product_size->prepareData($_POST)->delete();
