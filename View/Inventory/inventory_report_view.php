<?php
include_once('../../vendor/autoload.php');
use App\Sales\Sales;
use App\Purchase\Purchase;


$purchase = new Purchase();
$getAllPurchase = $purchase->prepareData($_GET)->index();

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <link rel="shortcut icon" href="../assets/images/favicon_1.ico">

    <title>POS- Point Of Sells Managment System</title>

    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you View the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="../assets/js/modernizr.min.js"></script>
    <style type="text/css" media="print" >
        .side-menu{display:none;} /*class for the element we don’t want to print*/
        .table-responsive{margin: 10px 10px 10px 10px;}
    </style>

</head>


<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <?php include_once('../header.php'); ?>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->

    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
            <!--- Divider -->
            <?php include_once('../menu.php'); ?>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- Left Sidebar End -->



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page" id="donotprintdiv">
        <!-- Start content -->
        <div class="content">
            <div class="container">


                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <!-- <div class="panel-heading">
                                <h4>Invoice</h4>
                            </div> -->

                            <style>
                                .logo-center{text-align: center;}

                            </style>
                            <div class="panel-body">
                                <div class="clearfix">
                                    <div class="col-md-12">

                                        <h4 class="logo-center"><img src="../assets/images/logo_dark.png" alt="velonic" ></h4>

                                    </div>


                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="logo-center">
                                            <address>

                                                795 Folsom Ave, Suite 600,
                                                San Francisco, CA 94107,
                                                <abbr title="Phone">P:</abbr> (123) 456-7890
                                            </address>
                                            <h4 class="page-title">Inventory Report</h4>
                                        </div>
                                        <div class="pull-right m-t-30">
                                            <p><strong>Date: </strong> <?php
                                                echo date("d/m/Y")?></p>
                                         
                                        </div>
                                    </div>
                                </div>
                                <div class="m-h-50"></div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table m-t-30">
                                                <thead>
                                                <tr><th>#</th>
                                                    <th>Supplier Name</th>
                                                    <th>Product Category</th>
                                                    <th>Product Name</th>
                                                    <th>Product Size</th>
                                                    <th>Quantity</th>
                                                </tr></thead>
                                                <tbody>
                                                <?php
                                                $sl=0;
                                                foreach($getAllPurchase as $purchase){
                                                    $sl++;
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $sl ?></td>
                                                        <td><?php echo $purchase['supplier_id']; ?></td>
                                                        <td><?php echo $purchase['product_cat_id']; ?></td>
                                                        <td><?php echo $purchase['product_id']; ?></td>
                                                        <td><?php echo $purchase['product_size_id']; ?></td>
                                                        <td><?php echo $purchase['quantity']; ?></td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <hr>
                                <div class="hidden-print">
                                    <div class="pull-right">
                                        <button onclick="myFunction()"><i class="fa fa-print"></i></button>
                                        <a href="#" class="btn btn-primary waves-effect waves-light">Submit</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div> <!-- container -->

        </div> <!-- content -->

        <?php include_once('../footer.php');?>

    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


    <!-- Right Sidebar -->

    <!-- /Right-bar -->


</div>
<!-- END wrapper -->

<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/detect.js"></script>
<script src="../assets/js/fastclick.js"></script>
<script src="../assets/js/jquery.slimscroll.js"></script>
<script src="../assets/js/jquery.blockUI.js"></script>
<script src="../assets/js/waves.js"></script>
<script src="../assets/js/wow.min.js"></script>
<script src="../assets/js/jquery.nicescroll.js"></script>
<script src="../assets/js/jquery.scrollTo.min.js"></script>


<script src="../assets/js/jquery.core.js"></script>
<script src="../assets/js/jquery.app.js"></script>
<script>
    function myFunction() {
        window.print();
    }
</script>
</body>
</html>