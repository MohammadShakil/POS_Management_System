<?php
include_once('../../vendor/autoload.php');
use App\Product\Product;
use App\Purchase\Purchase;

/*$product = new Product();
$getAllProduct= $product->prepareData($_GET)->index();*/


$purchase = new Purchase();
$getAllPurchase = $purchase->prepareData($_GET)->index();


?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <link rel="shortcut icon" href="../assets/images/favicon_1.ico">

    <title>POS- Point Of Sells managment System</title>

    <link href="../assets/plugins/custombox/dist/custombox.min.css" rel="stylesheet">

    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="../assets/js/modernizr.min.js"></script>

</head>


<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <?php include_once('../header.php');?>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->

    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
            <!--- Divider -->
            <?php include_once('../menu.php'); ?>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- Left Sidebar End -->
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <div class="row">
                                <div class="col-sm-8">
                                    <form role="form">
                                        <div class="form-group contact-search m-b-30">
                                            <input type="text" id="search" class="form-control" placeholder="Search...">
                                            <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
                                        </div> <!-- form-group -->
                                    </form>
                                </div>
                                <div class="col-sm-4">
                                    <a href="#custom-modal" class="btn btn-default btn-md waves-effect waves-light m-b-30" data-animation="fadein" data-plugin="custommodal"
                                       data-overlaySpeed="200" data-overlayColor="#36404a"><i class="md md-add"></i> New Purchase</a>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-hover mails m-0 table table-actions-bar">
                                    <thead>
                                    <tr>
                                        <th>Sl</th>
                                        <th>Purchase ID</th>
                                        <th>Supplier Name</th>
                                        <th>Product Category</th>
                                        <th>Product Name</th>
                                        <th>Product Size</th>
                                        <th>Quantity</th>

                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php
                                    $sl=0;
                                    foreach($getAllPurchase as $purchase){
                                        $sl++;
                                        ?>

                                        <tr>
                                            <td>
                                                <?php echo $sl?>
                                            </td>

                                            <td>
                                                <a href="#"><?php echo $purchase["purchase_id"]?> </a>
                                            </td>

                                            <td>
                                                <?php echo $purchase["supplier_name"]?>
                                            </td>
                                            <td>
                                                <?php echo $purchase["category_name"]?>
                                            </td>
                                            <td>
                                                <?php echo $purchase["product_name"]?>
                                            </td>
                                            <td>
                                                <?php echo $purchase["product_size_name"]?>
                                            </td>
                                            <td>
                                                <?php echo $purchase["quantity"]?>
                                            </td>

                                            <td>
                                                <a href="user-edit.php?id=<?php echo $product['id']?>" class="table-action-btn"><i class="md md-edit"></i></a>
                                                <a href="../../model/user/user-delete.php?id=<?php echo $product['id']?>" class="table-action-btn"><i class="md md-close"></i></a>
                                            </td>
                                        </tr>
                                    <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
        <?php include_once('../footer.php'); ?>
    </div>
    <!-- Modal -->
    <div id="custom-modal" class="modal-demo">
        <button type="button" class="close" onclick="Custombox.close();">
            <span>&times;</span><span class="sr-only">Close</span>
        </button>
        <h4 class="custom-modal-title">New Purchase</h4>
        <div class="custom-modal-text text-left">
            <form class="form-horizontal" role="form" method="post" action="store.php">
                <div class="form-group">
                    <label class="col-md-2 control-label">Supplier Name</label>
                    <div class="col-md-4">
                        <select class="form-control"  name="supplierID[]">
                            <option>----Select----</option>
                            <?php foreach($getAllSupplier as $supplier) {  ?>
                                <option value="<?php echo  $supplier['supplier_id'] ?>"><?php echo $supplier['supplier_name'] ?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Product Category </label>
                    <div class="col-md-4">
                        <select class="form-control"  name="CategoryID[]">
                            <option>----Select----</option>
                            <?php foreach($getAllProductCat as $category) {  ?>
                                <option value="<?php echo  $category['category_id'] ?>"><?php echo $category['category_name'] ?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Product Name</label>
                    <div class="col-md-4">
                        <select class="form-control" name="product[]">
                            <option>----Select----</option>
                            <?php foreach($getAllProduct as $product) {?>
                                <option value="<?php echo $product['product_id'] ?>"><?php echo $product['product_name'] ?></option>
                            <?php  }  ?>
                        </select>

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Product Size</label>
                    <div class="col-md-4">
                        <select class="form-control" name="size[]">
                            <option>----Select----</option>
                            <?php foreach($getAllProductSize as $size) {?>
                                <option value="<?php echo $size['product_size_id'] ?>"><?php echo $size['product_size_name'] ?></option>
                            <?php  }  ?>
                        </select>

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Quantity</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="quantity" value="">
                    </div>
                </div>
                <!-- <div class="form-group">
                     <label class="col-md-2 control-label">Item Price</label>
                     <div class="col-md-6">
                         <input type="date" class="form-control" name="price" value="">
                     </div>
                 </div>-->
                <!-- <div class="form-group">
                     <label class="col-md-2 control-label">Purchase Date</label>
                     <div class="col-md-6">
                         <input type="date" class="form-control" name="purchaseDate" value="">
                     </div>
                 </div>-->
                <!-- <div class="form-group">
                     <label class="col-md-2 control-label">User ID</label>
                     <div class="col-md-6">
                         <input type="text" class="form-control" name="name" value="">
                     </div>
                 </div>
-->


                <div class="form-group ">
                    <div class="col-sm-offset-2 col-sm-9">
                        <button type="submit" class="btn btn-info waves-effect waves-light">Submit</button>
                    </div>
                </div>

            </form>
        </div>
    </div>


    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


    <!-- Right Sidebar -->

    <!-- /Right-bar -->

</div>
<!-- END wrapper -->



<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/detect.js"></script>
<script src="../assets/js/fastclick.js"></script>
<script src="../assets/js/jquery.slimscroll.js"></script>
<script src="../assets/js/jquery.blockUI.js"></script>
<script src="../assets/js/waves.js"></script>
<script src="../assets/js/wow.min.js"></script>
<script src="../assets/js/jquery.nicescroll.js"></script>
<script src="../assets/js/jquery.scrollTo.min.js"></script>


<script src="../assets/js/jquery.core.js"></script>
<script src="../assets/js/jquery.app.js"></script>

<!-- Modal-Effect -->
<script src="../assets/plugins/custombox/dist/custombox.min.js"></script>
<script src="../assets/plugins/custombox/dist/legacy.min.js"></script>




</body>
</html>