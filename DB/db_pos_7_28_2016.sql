-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 28, 2016 at 07:16 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_type_id` int(11) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `customer_address` varchar(255) NOT NULL,
  `customer_phone` varchar(15) NOT NULL,
  `customer_email` varchar(20) NOT NULL,
  `customer_status` varchar(10) NOT NULL,
  PRIMARY KEY (`customer_id`),
  KEY `customer_type_id` (`customer_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `customer_type_id`, `customer_name`, `customer_address`, `customer_phone`, `customer_email`, `customer_status`) VALUES
(2, 1, 'Rashed', 'Dhaka', '0167376819303', 'smart@gmail.com', '1');

-- --------------------------------------------------------

--
-- Table structure for table `customer_type`
--

CREATE TABLE IF NOT EXISTS `customer_type` (
  `customer_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_type` varchar(255) NOT NULL,
  `customer_type_des` varchar(255) NOT NULL,
  `deleted_at` varchar(255) NOT NULL,
  PRIMARY KEY (`customer_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `customer_type`
--

INSERT INTO `customer_type` (`customer_type_id`, `customer_type`, `customer_type_des`, `deleted_at`) VALUES
(1, 'Admin', 'Administrator Rashed', '');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `first_name`, `last_name`, `email`, `password`) VALUES
(1, 'mamun', 'rahaman', 'mamunur@gmail.com', '123456'),
(2, 'jashim', 'uddin', 'tania_ctg1@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e'),
(3, 'jashim', 'uddin', 'tania_ctg2@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e'),
(4, 'jashim', 'uddin', 'tania_ctg5@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e'),
(5, 'Smart', 'Rashed', 'smartrashed@yahoo.com', '81dc9bdb52d04dc20036dbd8313ed055');

-- --------------------------------------------------------

--
-- Table structure for table `producat_category`
--

CREATE TABLE IF NOT EXISTS `producat_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(20) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `producat_category`
--

INSERT INTO `producat_category` (`category_id`, `category_name`) VALUES
(4, 'Laptop'),
(5, 'Food'),
(6, 'Book'),
(7, 'Desktop');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_code` varchar(255) NOT NULL,
  `product_size_id` int(11) NOT NULL,
  `product_price` int(255) NOT NULL,
  `product_sell_price` varchar(255) NOT NULL,
  `manufac_date` date NOT NULL,
  `exp_date` date NOT NULL,
  PRIMARY KEY (`product_id`),
  KEY `category_id` (`category_id`,`product_size_id`),
  KEY `category_id_2` (`category_id`,`product_size_id`),
  KEY `product_size_id` (`product_size_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `category_id`, `product_name`, `product_code`, `product_size_id`, `product_price`, `product_sell_price`, `manufac_date`, `exp_date`) VALUES
(1, 4, 'HP-1000', 'L20330', 3, 2000, '2222', '2016-07-01', '2016-07-30'),
(8, 4, 'DELL-100', 'D-100', 3, 400, '450', '0000-00-00', '0000-00-00'),
(10, 6, 'Quran', '786', 5, 400, '450', '0000-00-00', '0000-00-00'),
(11, 6, 'DELL-100', '56789', 4, 1, '450', '0000-00-00', '0000-00-00'),
(12, 6, 'DELL-100', '56789', 4, 10000, '450', '0000-00-00', '0000-00-00'),
(13, 6, 'DELL-100', '56789', 4, 10000, '450', '0000-00-00', '0000-00-00'),
(14, 5, '10-11-11', '56789', 5, 10000, '450', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `product_size`
--

CREATE TABLE IF NOT EXISTS `product_size` (
  `product_size_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_size_name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`product_size_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `product_size`
--

INSERT INTO `product_size` (`product_size_id`, `product_size_name`, `description`) VALUES
(3, '2 KG', 'Onion'),
(4, '15&#34;', 'Laptop'),
(5, 'Small', 'Bokk');

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE IF NOT EXISTS `purchase` (
  `purchase_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL,
  `product_cat_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_size_id` int(11) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `purchase_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`purchase_id`),
  KEY `product_cat_id` (`product_cat_id`),
  KEY `supplier_id` (`supplier_id`,`product_cat_id`,`product_id`,`product_size_id`),
  KEY `supplier_id_2` (`supplier_id`),
  KEY `product_cat_id_2` (`product_cat_id`),
  KEY `product_id` (`product_id`),
  KEY `product_size_id` (`product_size_id`),
  KEY `product_id_2` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `purchase`
--

INSERT INTO `purchase` (`purchase_id`, `supplier_id`, `product_cat_id`, `product_id`, `product_size_id`, `quantity`, `purchase_date`) VALUES
(10, 1, 6, 10, 5, '10', '0000-00-00 00:00:00'),
(11, 1, 6, 10, 5, '10', '0000-00-00 00:00:00'),
(12, 1, 6, 10, 5, '10', '0000-00-00 00:00:00'),
(13, 1, 6, 10, 5, '10', '0000-00-00 00:00:00'),
(14, 1, 6, 10, 5, '10', '0000-00-00 00:00:00'),
(15, 1, 6, 10, 5, '10', '0000-00-00 00:00:00'),
(16, 1, 7, 8, 5, '10', '0000-00-00 00:00:00'),
(17, 1, 7, 1, 4, '30', '0000-00-00 00:00:00'),
(18, 1, 5, 1, 5, '10', '0000-00-00 00:00:00'),
(19, 1, 5, 1, 5, '10', '0000-00-00 00:00:00'),
(20, 1, 7, 10, 5, '20', '0000-00-00 00:00:00'),
(21, 1, 5, 8, 5, '1', '0000-00-00 00:00:00'),
(22, 1, 5, 8, 5, '1', '0000-00-00 00:00:00'),
(23, 1, 5, 8, 5, '1', '0000-00-00 00:00:00'),
(24, 1, 6, 11, 5, '1', '0000-00-00 00:00:00'),
(25, 1, 6, 10, 5, '20', '0000-00-00 00:00:00'),
(27, 6, 6, 10, 5, '2', '0000-00-00 00:00:00'),
(28, 6, 6, 10, 5, '2', '0000-00-00 00:00:00'),
(29, 1, 4, 8, 3, '10', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE IF NOT EXISTS `sales` (
  `sale_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_size_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `sale_price` varchar(255) NOT NULL,
  `vat` varchar(255) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `sale_date` date NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`sale_id`),
  KEY `category_id` (`category_id`,`product_id`,`product_size_id`,`customer_id`,`user_id`),
  KEY `category_id_2` (`category_id`),
  KEY `product_id` (`product_id`),
  KEY `product_size_id` (`product_size_id`),
  KEY `customer_id` (`customer_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`sale_id`, `category_id`, `product_id`, `product_size_id`, `customer_id`, `customer_name`, `sale_price`, `vat`, `quantity`, `sale_date`, `user_id`) VALUES
(13, 4, 8, 3, 2, '', '2000', '15%', '2', '0000-00-00', 1),
(15, 5, 8, 4, 2, '', '45000', '15%', '1', '0000-00-00', 1),
(16, 5, 8, 4, 2, '', '45000', '15%', '1', '0000-00-00', 1),
(17, 5, 8, 4, 2, '', '45000', '15%', '1', '0000-00-00', 1),
(18, 4, 1, 4, 2, '', '2000', '15%', '10', '0000-00-00', 1),
(19, 4, 1, 4, 2, '', '2000', '15%', '10', '0000-00-00', 1),
(20, 7, 10, 5, 2, '', '2000', '15%', '30', '0000-00-00', 1),
(21, 7, 10, 5, 2, '', '2000', '15%', '30', '0000-00-00', 1),
(22, 6, 8, 4, 2, '', '2000', '15%', '30', '0000-00-00', 1),
(23, 4, 8, 4, 2, '', '45000', '15%', '2', '0000-00-00', 1),
(25, 5, 10, 3, 2, '', '45000', '15%', '1', '0000-00-00', 1),
(26, 4, 1, 3, 2, '', '45000', '15%', '1', '0000-00-00', 1),
(27, 4, 8, 4, 2, '', '45000', '15%', '1', '0000-00-00', 1),
(28, 4, 1, 5, 2, '', '2000', '15%', '10', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE IF NOT EXISTS `stock` (
  `stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(11) DEFAULT NULL,
  `sales_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`stock_id`),
  KEY `purchase_id` (`purchase_id`),
  KEY `sales_id` (`sales_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`stock_id`, `purchase_id`, `sales_id`, `product_id`) VALUES
(1, 11, 15, 10);

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE IF NOT EXISTS `suppliers` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(255) NOT NULL,
  `supplier_address` varchar(255) NOT NULL,
  `supplier_phone` varchar(255) NOT NULL,
  `supplier_email` varchar(255) NOT NULL,
  `supplier_bank_account` varchar(255) NOT NULL,
  `supplier_status` varchar(255) NOT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`supplier_id`, `supplier_name`, `supplier_address`, `supplier_phone`, `supplier_email`, `supplier_bank_account`, `supplier_status`) VALUES
(1, 'Rashed', 'Noakhali', '01673760122', 'smartrashed@yahoo.com', '564789', 'Good'),
(6, 'Rafi', 'dhaka', '01673760122', 'smartrashe@gmail.com', 'AB BANK', 'Good');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_type_id` (`user_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_type_id`, `user_name`, `user_email`, `user_pass`) VALUES
(1, 1, 'smartrashed', 'smartrashed@gmail.com', '123456');

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE IF NOT EXISTS `user_type` (
  `user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_name` varchar(255) NOT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`user_type_id`, `user_type_name`) VALUES
(1, 'Smart Rashed');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_ibfk_2` FOREIGN KEY (`customer_type_id`) REFERENCES `customer_type` (`customer_type_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_4` FOREIGN KEY (`product_size_id`) REFERENCES `product_size` (`product_size_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_ibfk_3` FOREIGN KEY (`category_id`) REFERENCES `producat_category` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `purchase`
--
ALTER TABLE `purchase`
  ADD CONSTRAINT `purchase_ibfk_6` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`supplier_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_ibfk_7` FOREIGN KEY (`product_cat_id`) REFERENCES `producat_category` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_ibfk_8` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_ibfk_9` FOREIGN KEY (`product_size_id`) REFERENCES `product_size` (`product_size_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `sales_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user_type` (`user_type_id`),
  ADD CONSTRAINT `sales_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `producat_category` (`category_id`),
  ADD CONSTRAINT `sales_ibfk_3` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`),
  ADD CONSTRAINT `sales_ibfk_4` FOREIGN KEY (`product_size_id`) REFERENCES `product_size` (`product_size_id`),
  ADD CONSTRAINT `sales_ibfk_5` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`);

--
-- Constraints for table `stock`
--
ALTER TABLE `stock`
  ADD CONSTRAINT `stock_ibfk_1` FOREIGN KEY (`purchase_id`) REFERENCES `purchase` (`purchase_id`),
  ADD CONSTRAINT `stock_ibfk_2` FOREIGN KEY (`sales_id`) REFERENCES `sales` (`sale_id`),
  ADD CONSTRAINT `stock_ibfk_3` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`user_type_id`) REFERENCES `user_type` (`user_type_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
