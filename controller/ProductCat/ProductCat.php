<?php
namespace App\ProductCat;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use MongoDB\Driver\Query;

Class ProductCat extends  DB
{
    public $id = "";
    public $name = "";



    public function __construct()
    {
        parent::__construct();
    }

    public function prepareData($data=array())
    {
        if (array_key_exists('name', $data)) {
            $this->name = filter_var($data['name'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function create()
    {
        $query = "INSERT INTO `db_pos`.`product_category` (`category_name`) VALUES ('{$this->name}')";
        Utility::dd($query);
        $result = mysqli_query($this->conn, $query);
        if ($result) {
           // echo "Done";
            //Message::message("<div class='alert alert-success'><strong>Success !</strong> Successfully Category Added</div>");
            //header('Location:../../view/ProductCat/product-category-list.php');
        } else {
            echo "Not Done";
          // Message::message("<div class='alert alert-success'><strong>Problem !</strong> Din not Successfully Inserted</div>");
          // header('Location:../../view/ProductCat/product-cat-create.php');
        }
    }
    public function index(){
        $list_data = array();
        $query = "SELECT * FROM `product_category`";
        $result = mysqli_query($this->conn, $query);
        while($row = mysqli_fetch_assoc($result)){
            $list_data[]=$row;
        }
        return $list_data;
    }
    public function view(){
        $query = "SELECT * FROM `product_category` WHERE `category_id`=".$this->id;
        $result = mysqli_query($this->conn,$query);
        $row = mysqli_fetch_assoc($result);
        if($row){
            return $row;
        }

    }


    public function update(){
        $query = "UPDATE `db_pos`.`product_category` SET `category_name` = '".$this->name."' WHERE `product_category`.`category_id` =".$this->id;
        $result = mysqli_query($this->conn, $query);

        if ($result) {

            Message::message("<div class='alert alert-success'><strong>Success !</strong> Successfully Updated</div>");
            header('Location:../../view/ProductCat/product-category-list.php');
        } else {
            Message::message("<div class='alert alert-success'><strong>Problem !</strong> Din not Successfully Updated</div>");
            header('Location:../../view/ProductCat/product-category-list.php');
        }
    }

    public function delete(){
        $query="DELETE FROM `db_pos`.`product_category` WHERE `producat_category`.`category_id` =".$this->id;
        /*echo $query;
        die();*/
        $result= mysqli_query($this->conn, $query);
        if ($result) {

            Message::message("<div class='alert alert-success'><strong>Success !</strong> Successfully Deleted</div>");
            header('Location:../../view/ProductCat/product-category-list.php');
        } else {

            Message::message("<div class='alert alert-success'><strong>Problem Occured !</strong> Din not Successfully Deleted</div>");
            header('Location:../../view/ProductCat/product-category-list.php');
        }
    }
}