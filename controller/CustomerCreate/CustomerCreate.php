<?php
namespace App\CustomerCreate;
use App\Message\Message;

class CustomerCreate
{
    public $customer_id;
    public $customer="";
    public $customer_name="";
    public $customer_address="";
    public $customer_phone="";
    public $customer_email="";
    public $customer_status="";
    public $conn;


    public function prepare($data = array())
    {
        if (array_key_exists("customer", $data)) {
            $this->customer = filter_var($data["customer"], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists("customer_name", $data)) {
            $this->customer_name = filter_var($data["customer_name"], FILTER_SANITIZE_STRING);
        }

        if (array_key_exists("customer_address", $data)) {
            $this->customer_address = $data['customer_address'];
        }
        if (array_key_exists("customer_phone", $data)) {
        $this->customer_phone = filter_var($data["customer_phone"], FILTER_SANITIZE_STRING);
    }

        if (array_key_exists("customer_email", $data)) {
            $this->customer_email = $data['customer_email'];
        }
        if (array_key_exists("customer_status", $data)) {
            $this->customer_status = filter_var($data["customer_status"], FILTER_SANITIZE_STRING);
        }

        if (array_key_exists("customer_id", $data)) {
            $this->customer_id = $data['customer_id'];
        }


        return $this;

    }


    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "db_pos");
    }

    public function store()
    {
        $query="INSERT INTO `db_pos`.`customers` (`customer_type_id`, `customer_name`, `customer_address`, `customer_phone`, `customer_email`, `customer_status`) VALUES ('".$this->customer."', '".$this->customer_name."', '".$this->customer_address."', '".$this->customer_phone."', '".$this->customer_email."', '".$this->customer_status."')";
       /* echo $query;
       die();*/
        $result = mysqli_query($this->conn, $query);
        if ($result) {
         
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been stored successfully.
</div>");
           header("location:../../View/Customer/customer-list.php");
        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Data has been not stored successfully.
</div>");
            
            header("location:../../View/Customer/customer-list.php");
        }
    }

    public function index()
    {
        $_allInfo = array();
        $query = "SELECT * FROM `customers`";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allInfo[] = $row;
        }
        return $_allInfo;
    }


    public function view()
    {
        $query = "SELECT * FROM `customers WHERE `id`=" . $this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }


    public function update()
    {
        $query = "UPDATE `db_pos`.`customers` SET `customer_name` = '" . $this->customer_name. "' WHERE `customer_name`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been Updated successfully.
</div>");
            header('Location:../../View/Customer/customers-type-list.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been updated  successfully.
    </div>");
            header('Location:../../View/customers-type-list.php');
        }

    }


    public function delete()
    {
        $query = "DELETE FROM `db_pos`.`customers` WHERE `customers`.`customer_id` =" . $this->customer_id;
       // $query = "DELETE FROM `db_pos`.`customers` WHERE `customer_name`.`customer_id` =" . $this->id;
       /* echo $query;
        die();*/
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            header('Location:../../View/Customer/customer-list.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been deleted  successfully.
    </div>");
            header('Location:../../View/Customer/customer-list.php');


        }


    }









}