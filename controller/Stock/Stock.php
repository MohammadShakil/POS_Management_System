<?php
namespace App\Stock;

use App\Model\Database as DB;

class Stock extends DB {

    public $stock_id = "";
    public $purchase_id="";
    public $sales_id="";
    public $product_id="";

    public function __construct()
    {
        parent::__construct();
    }

    public function dataPrepare($data=array()){
        if(array_key_exists('purchase_id',$data )){
            $this->purchase_id = $data['purchase_id'];
        }
        if(array_key_exists('sales_id',$data )){
            $this->sales_id = $data['sales_id'];
        }
        if(array_key_exists('product_id',$data )){
            $this->product_id = $data['product_id'];
        }
        if(array_key_exists('stock_id',$data )){
            $this->stock_id = $data['stock_id'];
        }

    }

    public function insertPurchase(){
        {
            $query = "INSERT INTO `db_pos`.`stock` ( `purchase_id`, `product_id`) VALUES ('{$this->purchase_id}','{$this->product_id}');";
            //Utility::dd($query);
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                echo "Done";
                //Message::message("<div class='alert alert-success'><strong>Success !</strong> Successfully Category Added</div>");
                //header('Location:index.php');
            } else {
                echo "Not Done";
                //Message::message("<div class='alert alert-success'><strong>Problem !</strong> Din not Successfully Inserted</div>");
                // header('Location:index.php');
            }
        }
    }
}

