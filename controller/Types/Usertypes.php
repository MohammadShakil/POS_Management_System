<?php
namespace App\Types;
use App\Message\Message;
use App\Utility\Utility;

class Usertypes
{
    public $user_type_id = "";
    public $user_type_name = "";

    public $conn;



    public function prepare($data = "")
    {
        if (array_key_exists("user_type_name", $data)) {
            $this->user_type_name = filter_var($data['user_type_name'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists("date", $data)) {
            $this->date = filter_var($data['date'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists("user_type_id", $data)) {
            $this->user_type_id = $data['user_type_id'];
        }

        return $this;

    }

    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "db_pos") or die("Database connection failed");
    }

    public function store(){
        $query="INSERT INTO `db_pos`.`user_type` (`user_type_name`) VALUES ('".$this->user_type_name. "')";
        //echo $query;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been stored successfully.
</div>");
            header('Location:../../View/Usertype/users-type.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been stored successfully.
    </div>");
            Utility::redirect('../../View/Usertype/users-type-create.php');

        }
    }

    public function index(){
        $_allBirthday= array();
        $query="SELECT * FROM `user_type`";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_allBirthday[]=$row;
        }

        return $_allBirthday;
    }
    public function view(){
        $query="SELECT * FROM `user_type` WHERE `user_type_id`=".$this->user_type_id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;
    }

    public function update(){
        $query="UPDATE `db_pos`.`user_type` SET `user_type_name` = '".$this->user_type_name."' WHERE `user_type`.`user_type_id` =".$this->user_type_id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been Updated successfully.
</div>");
            header('Location:../View/Usertype/User-type.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been updated  successfully.
    </div>");
            Utility::redirect('../View/Usertype/User-type.php');

        }

    }

    public function delete(){
        $query="DELETE FROM `db_pos`.`user_type` WHERE `user_type`.`user_type_id` = ".$this->user_type_id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            header('Location:../View/Usertype/User-type.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been deleted successfully.
    </div>");
            Utility::redirect('../View/Usertype/User-type.php');

        }
    }




}