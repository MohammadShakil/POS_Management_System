<?php
namespace App\Purchase;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
Class Purchase extends  DB
{
    public $id = "";
    public $CategoryID = "";
    public $supplierID = "";
    public $purchaseID="";
    public $product = "";
    public $size = "";
    public $quantity = "";
    public $price = "";
    public $purchaseDate = "";
    public $userID="";

    public function __construct()
    {
        parent::__construct();
    }

    public function prepareData($data=array())
    {
        if (array_key_exists('CategoryID', $data)) {
            $this->CategoryID = filter_var($data['CategoryID'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('supplierID', $data)) {
            $this->supplierID = filter_var($data['supplierID'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('product', $data)) {
            $this->product = filter_var($data['product'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('size', $data)) {
            $this->size = filter_var($data['size'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('quantity', $data)) {
            $this->quantity = filter_var($data['quantity'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('price', $data)) {
            $this->price = filter_var($data['price'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('purchaseDate', $data)) {
            $this->purchaseDate = filter_var($data['purchaseDate'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('userID', $data)) {
            $this->userID = filter_var($data['userID'], FILTER_SANITIZE_STRING);
        }

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function create()
    {
        $this->purchaseDate = getdate();
        $query = "INSERT INTO `purchase`(`supplier_id` ,`product_cat_id`, `product_id`, `product_size_id`, `quantity`,`purchase_date`)  VALUES ('{$this->supplierID}','{$this->CategoryID}','{$this->product}','{$this->size}','{$this->quantity}' ,NOW())";
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            header('Location:purchase-list.php');
        } else {
            header('Location:purchase-list.php');
       
        }
      
    }
    public function index(){
        $list_data = array();
        //$query = "SELECT * FROM `purchase`";
        $query= "SELECT *
FROM product_category c, products p, product_size s, suppliers sp, purchase pr
WHERE c.category_id = pr.product_cat_id
AND s.product_size_id = pr.product_size_id
AND sp.supplier_id = pr.supplier_id
AND p.product_id = pr.product_id";
        $result = mysqli_query($this->conn, $query);
        while($row = mysqli_fetch_assoc($result)){
            $list_data[]=$row;
        }
        return $list_data;
    }
    public function getCategoryValue(){

        $query ="SELECT c.category_name, p.product_name FROM producat_category AS c LEFT JOIN products AS p ON c.category_id = p.category_id";
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }
    public function view(){
        $query = "SELECT * FROM `products` WHERE `id`=".$this->id;
        $result = mysqli_query($this->Conn,$query);
        $row = mysqli_fetch_object($result);
        return $row;
    }
    public function update(){
        $query = "UPDATE `products` SET `name`='".$this->name."' WHERE `products`.`id`=".$this->id;
        $result = mysqli_query($this->Conn, $query);
        if ($result) {

            Message::message("<div class='alert alert-success'><strong>Success !</strong> Successfully Updated</div>");
            header('Location:index.php');
        } else {
            Message::message("<div class='alert alert-success'><strong>Problem !</strong> Din not Successfully Updated</div>");
            header('Location:index.php');
        }
    }

    public function delete(){
        $query="DELETE FROM `products` WHERE `products`.`id`=".$this->id;
        $result= mysqli_query($this->Conn, $query);
        if ($result) {

            Message::message("<div class='alert alert-success'><strong>Success !</strong> Successfully Deleted</div>");
            header('Location:viewTrash.php');
        } else {
            Message::message("<div class='alert alert-success'><strong>Problem Occured !</strong> Din not Successfully Deleted</div>");
            header('Location:index.php');
        }
    }

    public function totalPurchaseItem(){
        $getItem=array();
        $query="SELECT purchase_date, product_id, SUM( quantity ) AS \"Total\"FROM purchase GROUP BY purchase_date, product_id ORDER BY purchase_date";
        $result=mysqli_query($this->conn, $query);
        while($row=mysqli_fetch_object($result)){
            $getItem[]=$row;
        }
        return $getItem;
    }

    public function totalPurchaseAmount(){
        $query = "SELECT SUM( price )FROM purchase";
        $result = mysqli_query($this->conn, $query);
        $row=mysqli_fetch_object($result);
        return $row;
    }
}