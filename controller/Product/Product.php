<?php
namespace App\Product;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use MongoDB\Driver\Query;

Class Product extends  DB
{
    public $id = "";
    public $CategoryID = "";
    public $purchaseID ="";
    public $saleID="";
    public $productID="";
    public $name = "";
    public $code = "";
    public $size = "";
    public $price = "";
    public $sell_price = "";
    public $manufacture = "";
    public $exprie = "";


    public function __construct()
    {
        parent::__construct();
    }

    public function prepareData($data=array())
    {
        if (array_key_exists('CategoryID', $data)) {
            $this->CategoryID = filter_var($data['CategoryID'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('name', $data)) {
            $this->name = filter_var($data['name'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('code', $data)) {
            $this->code = filter_var($data['code'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('size', $data)) {
            $this->size = filter_var($data['size'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('price', $data)) {
            $this->price = filter_var($data['price'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('sell_price', $data)) {
            $this->sell_price = filter_var($data['sell_price'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('manufacture', $data)) {
            $this->manufacture = filter_var($data['manufacture'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('exprie', $data)) {
            $this->exprie = filter_var($data['exprie'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function create()
    {
        $query = "INSERT INTO `db_pos`.`products` (`category_id`, `product_name`, `product_code`, `product_size_id`, `product_price`, `product_sell_price`, `manufac_date`, `exp_date`) VALUES ( '{$this->CategoryID}', '{$this->name}', '{$this->code}', '{$this->size}', '{$this->price}', '{$this->sell_price}', '{$this->manufacture}', '{$this->exprie}');";

        //Utility::dd($query);
        $result = mysqli_query($this->conn, $query);
        if ($result) {

            Message::message("<div class='alert alert-success'><strong>Success !</strong> Successfully Category Added</div>");
            header('Location:../../View/Product/product-list.php');
        } else {

            Message::message("<div class='alert alert-success'><strong>Problem !</strong> Din not Successfully Inserted</div>");
           header('Location:../../View/Product/product-create.php');
        }
    }
    public function index(){
        $list_data = array();
        $query = "SELECT * FROM `products` ";
        $result = mysqli_query($this->conn, $query);
        while($row = mysqli_fetch_assoc($result)){
            $list_data[]=$row;
        }
        return $list_data;
    }
    public function getCategoryValue(){
        $list_data=array();
        //$query ="SELECT c.*, p.* FROM product_category AS c LEFT JOIN products AS p ON c.category_id = p.category_id";
        $query="SELECT *FROM product_category c, products p, product_size s WHERE c.category_id = p.category_id AND s.product_size_id = p.product_size_id";
        $result = mysqli_query($this->conn, $query);
        while($row = mysqli_fetch_assoc($result)){
            $list_data[]=$row;
        }
        return $list_data;
    }

    public function view(){
        $query = "SELECT * FROM `products` WHERE `product_id`=".$this->id;
        $result = mysqli_query($this->conn,$query);
        $row = mysqli_fetch_assoc($result);
        if($row){
            return $row;
        }

    }


    public function update(){
        $query = "UPDATE `db_pos`.`products` SET `category_name` = '".$this->name."' WHERE `products`.`product_id` =".$this->id;
        $result = mysqli_query($this->conn, $query);

        if ($result) {

            Message::message("<div class='alert alert-success'><strong>Success !</strong> Successfully Updated</div>");
            header('Location:../../view/ProductCat/product-category-list.php');
        } else {
            Message::message("<div class='alert alert-success'><strong>Problem !</strong> Din not Successfully Updated</div>");
            header('Location:../../view/ProductCat/product-category-list.php');
        }
    }

    public function delete(){
        $query="DELETE FROM `db_pos`.`products` WHERE `products`.`product_id` =".$this->id;
        /*echo $query;
        die();*/
        $result= mysqli_query($this->conn, $query);
        if ($result) {

            Message::message("<div class='alert alert-success'><strong>Success !</strong> Successfully Deleted</div>");
            header('Location:../../view/ProductCat/product-list.php');
        } else {

            Message::message("<div class='alert alert-success'><strong>Problem Occured !</strong> Din not Successfully Deleted</div>");
            header('Location:../../view/ProductCat/product-list.php');
        }
    }
}