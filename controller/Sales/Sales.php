<?php
namespace App\Sales;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use MongoDB\Driver\Query;

Class Sales extends  DB
{
    public $sale_id = "";
    public $CategoryID = "";
    public $product = "";
    public $supplierID = "";
    public $customer="";
    public $customerName="";
    public $size = "";
    public $quantity = "";
    public $salePrice = "";
    public $sales_date="";
    public $user_id="";
    public $discount="";
  

    public function __construct()
    {
        parent::__construct();
    }

    public function prepareData($data=array())
    {
        if (array_key_exists('CategoryID', $data)) {
            $this->CategoryID = filter_var($data['CategoryID'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('supplierID', $data)) {
            $this->supplierID = filter_var($data['supplierID'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('customerName', $data)) {
            $this->customerName = filter_var($data['customerName'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('customer', $data)) {
            $this->customer = filter_var($data['customer'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('product', $data)) {
            $this->product = filter_var($data['product'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('size', $data)) {
            $this->size = filter_var($data['size'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('quantity', $data)) {
            $this->quantity = filter_var($data['quantity'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('salePrice', $data)) {
            $this->salePrice = filter_var($data['salePrice'], FILTER_SANITIZE_STRING);
        }

        if (array_key_exists('sales_date', $data)) {
            $this->sales_date = filter_var($data['sales_date'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('user_id', $data)) {
            $this->user_id = filter_var($data['user_id'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('discount', $data)) {
            $this->discount = filter_var($data['discount'], FILTER_SANITIZE_STRING);
        }

        if (array_key_exists('sale_id', $data)) {
            $this->sale_id = $data['sale_id'];
        }
        return $this;
    }

    public function create()
    {
       
        $query = "INSERT INTO `sales` (`category_id`, `product_id`, `product_size_id`, `customer_id`, `sale_price`, `vat`, `quantity`,`discount` ,`sale_date`, `user_id`) 
VALUES ('{$this->CategoryID}','{$this->product}',{$this->size},'{$this->customer}','{$this->salePrice}','15%','{$this->quantity}','{$this->discount}', NOW(),'1')";
             $result = mysqli_query($this->conn, $query);
        if ($result) {
           
           header('Location:sales-list.php');

        } else {
         
            header('Location:sales-list.php');
        }
    }
   
    public function index(){
        $list_data = array();
       
        $query="SELECT * FROM product_category c, products p, product_size s, customers cs, sales sl WHERE c.category_id = sl.category_id
AND s.product_size_id = sl.product_size_id AND cs.customer_id = sl.customer_id AND p.product_id = sl.product_id ";
        $result = mysqli_query($this->conn, $query);
        while($row = mysqli_fetch_object($result)){
            $list_data[]=$row;
        }
        return $list_data;
    }
    public function saleInvoice(){
        $query = "SELECT * FROM `sales` WHERE `sale_id` = ".$this->sale_id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_object($result);
       return $row;
    }
   public function getCategoryValue(){
        $list_data = array();
        $query ="SELECT c.category_name, p.product_name FROM producat_category AS c LEFT JOIN products AS p ON c.category_id = p.category_id";
        $result = mysqli_query($this->conn, $query);
        while($row = mysqli_fetch_assoc($result)){
            $list_data[]=$row;
        }
        return $list_data;
    }
    public function view(){
        $query = "SELECT * FROM `products` WHERE `id`=".$this->id;
        $result = mysqli_query($this->Conn,$query);
        $row = mysqli_fetch_object($result);
        return $row;
    }
    public function update(){
        $query = "UPDATE `products` SET `name`='".$this->name."' WHERE `products`.`id`=".$this->id;
        $result = mysqli_query($this->Conn, $query);
        if ($result) {

            Message::message("<div class='alert alert-success'><strong>Success !</strong> Successfully Updated</div>");
            header('Location:sales-list.php');
        } else {
            Message::message("<div class='alert alert-success'><strong>Problem !</strong> Din not Successfully Updated</div>");
            header('Location:sales-list.php');
        }
    }

    public function delete(){
        $query="DELETE FROM `products` WHERE `products`.`id`=".$this->id;
        $result= mysqli_query($this->conn, $query);
        if ($result) {

            Message::message("<div class='alert alert-success'><strong>Success !</strong> Successfully Deleted</div>");
            header('Location:sales-list.php');
        } else {
            Message::message("<div class='alert alert-success'><strong>Problem Occured !</strong> Din not Successfully Deleted</div>");
            header('Location:sales-list.php');
        }
    }

    public function totalSalesItem(){
        $getItem=array();
        $query="SELECT sale_date, product_id, SUM( quantity ) AS \"Total\" FROM sales GROUP BY sale_date, product_id ORDER BY sale_date";
        $result=mysqli_query($this->conn, $query);
        while($row=mysqli_fetch_object($result)){
            $getItem[]=$row;
        }
        return $getItem;
    }
    public function totalSalesAmount(){
        $query = "SELECT  SUM( sale_price ) FROM sales";
        $result = mysqli_query($this->conn, $query);
        $row=mysqli_fetch_object($result);
        return $row;
    }

}