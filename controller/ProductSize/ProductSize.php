<?php
namespace App\ProductSize;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;


Class ProductSize extends  DB
{
    public $product_size_id = "";
    public $product_size_id2 = "";
    public $name="";
    public $description="";



    public function __construct()
    {
        parent::__construct();
    }

    public function prepareData($data=array())
    {
        if (array_key_exists('name', $data)) {
            $this->name = filter_var($data['name'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('description', $data)) {
            $this->description = filter_var($data['description'], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists('product_size_id2', $data)) {
            $this->product_size_id2 = $data['product_size_id2'];
        }
        if (array_key_exists('product_size_id', $data)) {
            $this->product_size_id = $data['product_size_id'];
        }
        return $this;
       //die();
    }

    public function create()
    {
        $query = "INSERT INTO `product_size` (`product_size_name`,`description`) VALUES ('{$this->name}','{$this->description}')";

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            //echo "Done";
            Message::message("<div class='alert alert-success'><strong>Success !</strong> Successfully Category Added</div>");
            header('Location:../../view/ProductSize/product-size-list.php');
        } else {
            //echo "Not Done";
           Message::message("<div class='alert alert-success'><strong>Problem !</strong> Din not Successfully Inserted</div>");
           header('Location:../../view/ProductSize/product-size-create.php');
        }
    }
    public function index(){
        $list_data = array();
        $query = "SELECT * FROM `product_size`";
        $result = mysqli_query($this->conn, $query);
        while($row = mysqli_fetch_assoc($result)){
            $list_data[]=$row;
        }
        return $list_data;
    }
    public function view(){
        $query = "SELECT * FROM `product_size` WHERE `product_size_id`=".$this->product_size_id2;
        $result = mysqli_query($this->conn,$query);
        $row = mysqli_fetch_assoc($result);
        if($row){
            return $row;
        }

    }
    public function update(){
        $query="UPDATE `db_pos`.`product_size` SET `product_size_name` = '".$this->name."', `description` ='".$this->description."' WHERE `product_size`.`product_size_id`=".$this->product_size_id;
      /* echo $query;
        die();*/
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been Updated successfully.
</div>");
            header('Location:../../view/ProductSize/product-size-list.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been updated  successfully.
    </div>");
            Utility::redirect('../../view/ProductSize/product-size-list.php');

        }

    }

    public function delete(){
        $query="DELETE FROM `db_pos`.`product_size` WHERE `product_size`.`product_size_id` =".$this->product_size_id;
        $result= mysqli_query($this->conn, $query);
        if ($result) {

            Message::message("<div class='alert alert-success'><strong>Success !</strong> Successfully Deleted</div>");
            header('Location:../../view/ProductSize/product-size-list.php');
        } else {
            
            Message::message("<div class='alert alert-success'><strong>Problem Occured !</strong> Din not Successfully Deleted</div>");
            header('Location:../../view/ProductSize/product-size-list.php');
        }
    }
}