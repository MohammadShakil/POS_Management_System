<?php
namespace App\Customer;
use App\Message\Message;

class CustomerType
{
    public $id;
    public $customer_type;
    public $customer_type_des;
    public $conn;


    public function prepare($data = array())
    {
        if (array_key_exists("customer_type", $data)) {
            $this->customer_type = filter_var($data["customer_type"], FILTER_SANITIZE_STRING);
        }
        if (array_key_exists("customer_type_des", $data)) {
                $this->customer_type_des = filter_var($data["customer_type_des"], FILTER_SANITIZE_STRING);
            }
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }

        return $this;

    }


    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "db_pos");
    }

    public function store()
    {
        $query="INSERT INTO `db_pos`.`customer_type` (`customer_type`, `customer_type_des`) VALUES ('".$this->customer_type."', '".$this->customer_type_des."')";
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been stored successfully.
</div>");
            header("location:../../View/customers-type-list.php");
        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Data has been not stored successfully.
</div>");
            header("location:../../View/customer-type.php");
        }
    }

    public function index()
    {
        $_allInfo = array();
        $query = "SELECT * FROM `customer_type`";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allInfo[] = $row;
        }
        return $_allInfo;
    }


    public function view()
    {
        $query = "SELECT * FROM `customer_type` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }


    public function update()
    {
        $query = "UPDATE `db_pos`.`customer_type` SET `customer_type` = '" . $this->customer_type . "' WHERE `customer_type`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been Updated successfully.
</div>");
            header('Location:../../View/customers-type-list.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been updated  successfully.
    </div>");
            header('Location:../../View/customers-type-list.php');
        }

    }

    public function delete()
    {
        $query = "DELETE FROM `db_pos`.`customer_type` WHERE `customer_type`.`id` =" . $this->id;
       
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            header('Location:../../View/customers-type-list.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been deleted  successfully.
    </div>");
            header('Location:../../View/customers-type-list.php');


        }


    }


  


    

    

}