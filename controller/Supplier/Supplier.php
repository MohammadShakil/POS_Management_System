<?php
namespace App\Supplier;
use App\Message\Message;

class Supplier
{
    public $supplier_id;
    public $supplier_name;
    public $supplier_address;
    public $supplier_phone;
    public $supplier_email;
    public $supplier_bank_account;
    public $supplier_status;
    public $conn;
    public $filterByName= "";


    public function prepare($data =array())
    {
        if (array_key_exists("supplier_name", $data)) {
            $this->supplier_name = $data['supplier_name'];
        }

        if (array_key_exists("supplier_address", $data)) {
            $this->supplier_address = $data['supplier_address'];
        }
        if (array_key_exists("supplier_phone", $data)) {
            $this->supplier_phone = $data['supplier_phone'];
        }

        if (array_key_exists("supplier_email", $data)) {
            $this->supplier_email = $data['supplier_email'];
        }
        if (array_key_exists("supplier_bank_account", $data)) {
            $this->supplier_bank_account = $data['supplier_bank_account'];
        }
        if (array_key_exists("supplier_status", $data)) {
            $this->supplier_status = $data['supplier_status'];
        }
        if (array_key_exists("search", $data)) {
            $this->search = $data['search'];
        }

        if (array_key_exists("supplier_id", $data)) {
            $this->supplier_id = $data['supplier_id'];
        }
        return $this;
    }

    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "db_pos");
    }

    public function store()
    {
        $query ="INSERT INTO `db_pos`.`suppliers` (`supplier_name`, `supplier_address`, `supplier_phone`, `supplier_email`,`supplier_bank_account` ,`supplier_status`) VALUES ('" . $this->supplier_name . "', '" . $this->supplier_address . "', '" . $this->supplier_phone . "','".$this->supplier_email."','" . $this->supplier_bank_account . "', '" . $this->supplier_status . "')";
        /* echo $query;
         die();*/
        $result = mysqli_query($this->conn, $query);
        if ($result) {

            Message::message("<div class=\"alert alert-success\">,
  <strong>Success!</strong> Data has been stored successfully.
</div>");
            header("location:../../view/Supplier/supplier-list.php");
        } else {

            Message::message("<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Data has been not stored successfully.
</div>");
            header("location:../../view/Supplier/supplier-list.php");
        }

    }
    public function index()
    { /*$whereClause=" 1=1 ";
        
        if(!empty($this->search)) {
            $whereClause .= " AND supplier_name LIKE '%".$this->search."%'";
        }*/
        $_allInfo = array();
        $query = "SELECT * FROM `suppliers` ";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allInfo[] = $row;
        }
        return $_allInfo;
    }

    public function delete()
    {
        $query = "DELETE FROM `db_pos`.`suppliers` WHERE `suppliers`.`supplier_id` =" . $this->supplier_id;
        /*echo $query;
        die();*/
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("<div class=\"alert alert-success\">,
  <strong>Success!</strong> Data has been stored successfully.
</div>");
            header("location:../../view/Supplier/supplier-list.php");
        } else {

            Message::message("<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Data has been not stored successfully.
</div>");
            header("location:../../view/Supplier/supplier-list.php");
        }
    }
    public function update()
    {
        $query ="UPDATE `db_pos`.`suppliers` SET `supplier_name` = '".$this->supplier_name."', `supplier_address` = '".$this->supplier_address."', `supplier_phone` = '".$this->supplier_phone."', `supplier_email` = '".$this->supplier_email."', `supplier_bank_account` = '".$this->supplier_bank_account."', `supplier_status` = '".$this->supplier_status."' WHERE `suppliers`.`supplier_id` =" .$this->supplier_id;
        /* echo $query;
         die();*/
        $result = mysqli_query($this->conn, $query);
        if($result){
            Message::message("<div class=\"alert alert-success\">,
  <strong>Success!</strong> Data has been stored successfully.
</div>");
            header("location:../../view/Supplier/supplier-list.php");
        }
        else{
            Message::message("<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Data has been not stored successfully.
</div>");
            header("location:../../view/Supplier/supplier-list.php");
        }

    }

    public function view()
    {
        $query = "SELECT * FROM `suppliers` WHERE `supplier_id`=" . $this->supplier_id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }
    public function getAllName(){
        $_allName= array();
        $query="SELECT supplier_name FROM `suppliers`";
        $result=mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_assoc($result)){
            $_allName[]=$row['supplier_name'];
        }

        return $_allName;
    }
}
